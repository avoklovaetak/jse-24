package ru.volkova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

import java.util.Collection;

public class HelpCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "show terminal commands";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands)
            System.out.println(command.name() + ": " + command.description());
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

}
