package ru.volkova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Ekaterina Volkova");
        System.out.println("E-MAIL: avoklovaetak@yandex.ru");
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

}
