package ru.volkova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

import java.util.Collection;

public class CommandsListCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show program commands";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[COMMANDS]");
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            final String name = command.name();
            if (name == null) continue;
            System.out.println(name);
        }
    }

    @NotNull
    @Override
    public String name() {
        return "commands-list";
    }

}
