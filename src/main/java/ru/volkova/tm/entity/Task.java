package ru.volkova.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.entity.IWBS;

@Getter
@Setter
public final class Task extends AbstractOwnerEntity implements IWBS {

    @Nullable
    private String projectId;

    @NotNull
    @Override
    public String toString() {
        return id + ": " + getName() + ": " + projectId
                + "; " + "created: " + getCreated() +
                "started: " + getDateStart();
    }

}
