package ru.volkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.entity.Project;

import java.util.Optional;

public interface IProjectRepository extends IOwnerRepository<Project> {

    @NotNull
    Optional<Project> add(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    );

}
