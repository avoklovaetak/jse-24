package ru.volkova.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.volkova.tm.api.repository.*;
import ru.volkova.tm.api.service.*;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.empty.EmptyCommandException;
import ru.volkova.tm.exception.system.UnknownArgumentException;
import ru.volkova.tm.repository.*;
import ru.volkova.tm.service.*;
import ru.volkova.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Set;

public class Bootstrap implements ServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ILogService logService = new LogService();

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.volkova.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.volkova.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    public void initData() {
        projectService.add("1", "DEMO 1", "1")
                .ifPresent(project -> project.setStatus(Status.NOT_STARTED));
        projectService.add("1", "DEMO 4", "1")
                .ifPresent(project -> project.setStatus(Status.NOT_STARTED));
        projectService.add("2", "DEMO 3", "1")
                .ifPresent(project -> project.setStatus(Status.COMPLETE));
        projectService.add("2", "DEMO 0", "1")
                .ifPresent(project -> project.setStatus(Status.IN_PROGRESS));

        taskService.add("1", "DEMO 1", "1")
                .ifPresent(task -> task.setStatus(Status.IN_PROGRESS));
        taskService.add("2", "DEMO 4", "1")
                .ifPresent(task -> task.setStatus(Status.IN_PROGRESS));
        taskService.add("2", "DEMO 5", "1")
                .ifPresent(task -> task.setStatus(Status.NOT_STARTED));
        taskService.add("1", "DEMO 8", "1")
                .ifPresent(task -> task.setStatus(Status.COMPLETE));
    }

    private void initUsers() {
        @NotNull final User user = userService.create("user1", "test", "user1@mail.ru");
        user.setId("1");
        @NotNull final User admin = userService.create("admin", "pass", Role.ADMIN);
        admin.setId("2");
    }

    public void parseArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) throw new UnknownArgumentException();
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    public boolean parseArgs(@Nullable String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(@Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyCommandException();
        @Nullable final AbstractCommand command = commandService.getCommandByName(name);
        if (command == null) return;
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(final String... args) {
        logService.info("*** WELCOME TO TASK MANAGER ***");
        initCommands();
        initData();
        initUsers();
        if (parseArgs(args)) System.exit(0);
        while (true) {
            System.out.println("ENTER COMMAND:");
            @NotNull final String command = TerminalUtil.nextLine();
            logService.command(command);
            try {
                parseCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                logService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

}
