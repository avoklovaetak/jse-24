package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IUserRepository;
import ru.volkova.tm.api.service.IUserService;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.auth.EmailExistsException;
import ru.volkova.tm.exception.auth.LoginExistsException;
import ru.volkova.tm.exception.empty.*;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.UserNotFoundException;
import ru.volkova.tm.util.HashUtil;

import java.util.List;
import java.util.Optional;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create
            (@Nullable final String login,
             @Nullable final String password,
             @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @NotNull
    public Optional<User> findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @NotNull
    @Override
    public Optional<User> findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    @NotNull
    public Optional<User> findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email).isPresent();
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login).isPresent();
    }

    @Override
    public void lockByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        userRepository.lockByEmail(email);
    }

    @Override
    public void lockById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.lockById(id);
    }

    @Override
    public void lockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.lockByLogin(login);
    }

    @Override
    public void remove(@Nullable final User user) {
        if (user == null) throw new ObjectNotFoundException();
        userRepository.remove(user);
    }

    @Override
    public void removeByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        userRepository.removeByEmail(email);
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.removeById(id);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.removeByLogin(login);
    }

    @NotNull
    public Optional<User> setPassword(
            @Nullable final String userId,
            @Nullable final String password
    ) {
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        final String hash = HashUtil.salt(password);
        user.get().setPasswordHash(hash);
        return user;
    }

    @Override
    public void unlockByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        userRepository.unlockByEmail(email);
    }

    @Override
    public void unlockById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.unlockById(id);
    }

    @Override
    public void unlockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.unlockByLogin(login);
    }

    @NotNull
    public Optional<User> updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String secondName,
            @Nullable final String middleName
    ) {
        @NotNull final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        if (firstName == null || firstName.isEmpty()
                || secondName == null || secondName.isEmpty()
                || middleName == null || middleName.isEmpty())
        user.get().setFirstName(firstName);
        user.get().setSecondName(secondName);
        user.get().setMiddleName(middleName);
        return user;
    }

}
