package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IOwnerRepository;
import ru.volkova.tm.api.service.IOwnerService;
import ru.volkova.tm.entity.AbstractOwnerEntity;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.empty.EmptyIdException;
import ru.volkova.tm.exception.empty.EmptyNameException;
import ru.volkova.tm.exception.empty.EmptyStatusException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.system.IndexIncorrectException;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity>
        extends AbstractService<E> implements IOwnerService<E> {

    @NotNull
    protected final IOwnerRepository<E> ownerRepository;

    protected AbstractOwnerService(@NotNull IOwnerRepository<E> repository) {
        super(repository);
        this.ownerRepository = repository;
    }

    @NotNull
    @Override
    public Optional<E> changeOneStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Optional<E> entity = findOneByName(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        return entity;
    }

    @NotNull
    @Override
    public Optional<E> changeOneStatusByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Optional<E> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        return entity;
    }

    @NotNull
    @Override
    public Optional<E> changeOneStatusByName(
            @NotNull final String userId,
            @Nullable String name,
            @Nullable Status status
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Optional<E> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        return entity;
    }

    @Override
    public void clear(@NotNull final String userId) {
        ownerRepository.clear(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return ownerRepository.findAll(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @Nullable final Comparator<E> comparator) {
        if (comparator == null) throw new ObjectNotFoundException();
        return ownerRepository.findAll(userId, comparator);
    }

    @NotNull
    @Override
    public Optional<E> findById(
            @NotNull final String userId,
            @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return ownerRepository.findById(userId, id);
    }

    @NotNull
    @Override
    public Optional<E> findOneByIndex(
            @NotNull final String userId,
            @Nullable final Integer index
    ) {
        if (index == null) throw new IndexIncorrectException();
        return ownerRepository.findOneByIndex(userId, index);
    }

    @NotNull
    @Override
    public Optional<E> findOneByName(
            @NotNull final String userId,
            @Nullable final String name
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return ownerRepository.findOneByName(userId, name);
    }

    @NotNull
    @Override
    public Optional<E> finishOneById(
            @NotNull final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Optional<E> entity = findById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        entity.get().setDateFinish(new Date());
        return entity;
    }

    @NotNull
    @Override
    public Optional<E> finishOneByIndex(
            @NotNull final String userId,
            @Nullable final Integer index
    ) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final Optional<E> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        entity.get().setDateFinish(new Date());
        return entity;
    }

    @NotNull
    @Override
    public Optional<E> finishOneByName(
            @NotNull final String userId,
            @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<E> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        entity.get().setDateFinish(new Date());
        return entity;
    }

    @Override
    public void removeById(@NotNull final String userId,@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        ownerRepository.removeById(userId, id);
    }

    @Override
    public void removeOneByIndex(@NotNull final String userId,@Nullable final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        ownerRepository.removeOneByIndex(userId, index);
    }

    @Override
    public void removeOneByName(@NotNull final String userId,@Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        ownerRepository.removeOneByName(userId, name);
    }

    @NotNull
    @Override
    public Optional<E> startOneById(
            @NotNull final String userId,
            @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Optional<E> entity = findById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        entity.get().setDateStart(new Date());
        return entity;
    }

    @NotNull
    @Override
    public Optional<E> startOneByIndex(
            @NotNull final String userId,
            @Nullable final Integer index
    ) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final Optional<E> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        entity.get().setDateStart(new Date());
        return entity;
    }

    @NotNull
    @Override
    public Optional<E> startOneByName(
            @NotNull final String userId,
            @Nullable final String name
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<E> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        entity.get().setDateStart(new Date());
        return entity;
    }

    @NotNull
    @Override
    public Optional<E> updateOneById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<E> entity = findById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        return entity;
    }

    @NotNull
    @Override
    public Optional<E> updateOneByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {

        @NotNull final Optional<E> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        return entity;
    }

}
