package ru.volkova.tm;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(@NotNull String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
